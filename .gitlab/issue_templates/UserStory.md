### Story

As a *user or stakeholder type*

I want *some software feature*

So that *some business value*

### Acceptance criteria

* *user or stakeholder type* is able to...
* *user or stakeholder type* can add...
* *user or stakeholder type* can remove...

/label ~User-Story
