use Mix.Config

config :un_notifications,
       UNNotifications.Emails.DefaultImpl.Mailer,
       adapter: Swoosh.Adapters.Sendgrid
