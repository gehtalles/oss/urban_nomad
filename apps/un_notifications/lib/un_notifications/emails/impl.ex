defmodule UNNotifications.Emails.Impl do
  @moduledoc false

  alias UrbanNomad.PasswordReset

  @type email_address :: UNNotifications.Emails.email_address()

  @callback send_welcome(email_address) :: :ok

  @callback send_password_reset(email_address, PasswordReset.t()) :: :ok
end
