defmodule UNNotifications.Emails do
  @moduledoc """
  Internal API for working with emails in the Urban Nomad app.
  """

  alias UNNotifications.Emails.DefaultImpl
  alias UrbanNomad.{PasswordReset}

  @behaviour UNNotifications.Emails.Impl

  @type email_address :: String.t()

  @doc """
  Sends the welcome email to the given email address.
  """
  @spec send_welcome(email_address) :: :ok
  def send_welcome(email_address) do
    impl().send_welcome(email_address)
  end

  @spec send_password_reset(email_address, PasswordReset.t()) :: :ok
  def send_password_reset(email_address, password_reset) do
    impl().send_password_reset(email_address, password_reset)
  end

  defp impl do
    Application.get_env(:un_notifications, :impl, DefaultImpl)
  end
end
