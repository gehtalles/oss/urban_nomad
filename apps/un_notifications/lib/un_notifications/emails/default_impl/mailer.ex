defmodule UNNotifications.Emails.DefaultImpl.Mailer do
  @moduledoc false
  use Swoosh.Mailer, otp_app: :un_notifications
end
