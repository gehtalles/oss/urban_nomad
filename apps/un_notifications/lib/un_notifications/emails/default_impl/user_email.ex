defmodule UNNotifications.Emails.DefaultImpl.UserEmail do
  @moduledoc false

  import Swoosh.Email

  @from {"Tech AT gehtalles", "tech@gehtalles.at"}

  def welcome(email) do
    new()
    |> to(email)
    |> from(@from)
    |> subject("Welcome to Tech")
    |> html_body("<h1>Thanks for signing up for Tech, #{email}!</h1>")
  end

  def password_reset(email, reset_code) do
    new()
    |> to(email)
    |> from(@from)
    |> subject("Password Reset Request")
    |> html_body("<h1>#{reset_code}</h1>")
  end
end
