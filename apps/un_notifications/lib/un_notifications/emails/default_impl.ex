defmodule UNNotifications.Emails.DefaultImpl do
  @moduledoc false

  alias UrbanNomad.PasswordReset

  alias UNNotifications.Emails.DefaultImpl.{
    Mailer,
    UserEmail
  }

  @behaviour UNNotifications.Emails.Impl

  @type email_address :: UNNotifications.Emails.email_address()

  @spec send_welcome(email_address) :: :ok
  def send_welcome(email_address) do
    email_address |> UserEmail.welcome() |> Mailer.deliver()
    :ok
  end

  @spec send_password_reset(email_address, PasswordReset.t()) :: :ok
  def send_password_reset(email_address, %PasswordReset{code: reset_code}) do
    email_address |> UserEmail.password_reset(reset_code) |> Mailer.deliver()
    :ok
  end
end
