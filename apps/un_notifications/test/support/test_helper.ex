defmodule UNNotifications.TestHelper do
  @moduledoc false

  alias UrbanNomad.PasswordReset

  def valid_password_reset_struct do
    %PasswordReset{code: random_string()}
  end

  defp random_string do
    8
    |> :crypto.strong_rand_bytes()
    |> Base.encode16()
    |> String.downcase()
  end
end
