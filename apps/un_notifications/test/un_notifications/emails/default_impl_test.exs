defmodule UNNotifications.Emails.DefaultImplTest do
  use ExUnit.Case, async: true

  import Swoosh.TestAssertions
  import UNNotifications.TestHelper

  alias UNNotifications.Emails.DefaultImpl
  alias UNNotifications.Emails.DefaultImpl.UserEmail

  test "send_welcome/1 sends the appropriate email" do
    email = "foo@example.com"

    assert :ok = DefaultImpl.send_welcome(email)

    assert_email_sent(UserEmail.welcome(email))
  end

  test "send_password_reset/1 sends the appropriate email" do
    email = "foo@example.com"
    password_reset = valid_password_reset_struct()

    assert :ok = DefaultImpl.send_password_reset(email, password_reset)

    assert_email_sent(UserEmail.password_reset(email, password_reset.code))
  end
end
