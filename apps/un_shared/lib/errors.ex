defmodule UrbanNomad.InvalidDataError do
  @moduledoc """
  Indicates a record could not be persisted due to invalid data.
  """

  @type t :: %__MODULE__{message: String.t(), errors: term}

  defexception [:message, :errors]

  def exception(opts) do
    errors = Keyword.fetch!(opts, :errors)

    message = """
    unable to persist record because data is invalid
    errors:
    #{inspect(errors)}
    """

    %__MODULE__{message: message, errors: errors}
  end
end

defmodule UrbanNomad.EmailAlreadyRegisteredError do
  @moduledoc """
  Indicates an email could not be used because it is already
  registered to another user.
  """

  @type t :: %__MODULE__{message: String.t(), email: String.t()}

  defexception [:message, :email]

  def exception(opts) do
    email = Keyword.fetch!(opts, :email)
    message = "#{email} is already registered"

    %__MODULE__{message: message, email: email}
  end
end

defmodule UrbanNomad.WrongCredentialsError do
  @moduledoc """
  Indicates a user could not sign in because Credentials are wrong.
  """

  @type t :: %__MODULE__{message: String.t(), errors: term}

  defexception [:message, :errors]

  def exception(opts) do
    errors = Keyword.fetch!(opts, :errors)

    message = """
    unable to sign in wrong credentials:
    #{inspect(errors)}
    """

    %__MODULE__{message: message, errors: errors}
  end
end

defmodule UrbanNomad.UserNotFoundError do
  @moduledoc """
  Indicates that a user could not be found.
  """

  @type t :: %__MODULE__{message: String.t(), error: String.t()}

  defexception [:message, :error]

  def exception(opts) do
    error = Keyword.fetch!(opts, :error)

    message = """
    User could not found:
    #{error}
    """

    %__MODULE__{message: message, error: error}
  end
end
