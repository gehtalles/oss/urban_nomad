defmodule UrbanNomad.PasswordReset do
  @moduledoc """
  Passwort Reset struct. Contains a code to update a user password.
  """

  @type t :: %__MODULE__{code: String.t()}

  defstruct [:code]
end
