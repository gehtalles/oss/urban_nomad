defmodule UrbanNomad.User do
  @moduledoc """
  A user struct.
  """

  @type t :: %__MODULE__{id: String.t(), email: String.t(), display_name: String.t()}

  # @enforce_keys [:id, :email, :display_name]
  defstruct [:id, :email, :display_name]
end
