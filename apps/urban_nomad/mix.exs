defmodule UrbanNomad.MixProject do
  use Mix.Project

  def project do
    [
      app: :urban_nomad,
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps: deps(),
      deps_path: "../../deps",
      elixir: "~> 1.7",
      elixirc_paths: elixirc_paths(Mix.env()),
      lockfile: "../../mix.lock",
      start_permanent: Mix.env() == :prod,
      version: "0.1.0"
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:un_accounts, in_umbrella: true},
      {:un_notifications, in_umbrella: true},
      {:mox, "~> 0.4", only: :test}
    ]
  end
end
