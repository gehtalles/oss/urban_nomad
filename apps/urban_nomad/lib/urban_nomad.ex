defmodule UrbanNomad do
  @moduledoc """
  Urban Nomad Public API.
  """

  alias UrbanNomad.{
    DefaultImpl,
    EmailAlreadyRegisteredError,
    InvalidDataError,
    PasswordReset,
    User,
    UserNotFoundError,
    WrongCredentialsError
  }

  @behaviour UrbanNomad.Impl

  @doc """
  Register a user.
  """
  @spec register_user(map) ::
          {:ok, User.t()} | {:error, EmailAlreadyRegisteredError.t() | InvalidDataError.t()}
  def register_user(user_params), do: impl().register_user(user_params)

  @doc """
  Authenticate an user.
  """
  @spec authenticate_user(map) :: {:ok, User.t()} | {:error, WrongCredentialsError.t()}
  def authenticate_user(user_credentials), do: impl().authenticate_user(user_credentials)

  @doc """
  Get an user by id
  """
  @spec get_user_by_id(String.t()) :: {:ok, User.t()} | {:error, UserNotFoundError.t()}
  def get_user_by_id(user_id), do: impl().get_user_by_id(user_id)

  @spec initiate_password_reset(String.t()) ::
          {:ok, PasswordReset.t()} | {:error, UserNotFoundError.t()}
  def initiate_password_reset(email), do: impl().initiate_password_reset(email)

  @spec validate_password_reset_code(String.t()) ::
          {:ok, PasswordReset.t()} | {:error, InvalidDataError.t()}
  def validate_password_reset_code(password_reset_code),
    do: impl().validate_password_reset_code(password_reset_code)

  @spec update_user_password(String.t(), map) :: {:ok, User.t()} | {:error, InvalidDataError.t()}
  def update_user_password(password_reset_code, update),
    do: impl().update_user_password(password_reset_code, update)

  defp impl do
    Application.get_env(:urban_nomad, :impl, DefaultImpl)
  end
end
