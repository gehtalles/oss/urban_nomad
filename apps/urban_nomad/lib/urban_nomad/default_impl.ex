defmodule UrbanNomad.DefaultImpl do
  @moduledoc false

  alias UrbanNomad.{
    EmailAlreadyRegisteredError,
    InvalidDataError,
    PasswordReset,
    User,
    UserNotFoundError,
    WrongCredentialsError
  }

  alias UNNotifications.Emails

  @behaviour UrbanNomad.Impl

  @spec register_user(map) ::
          {:ok, User.t()} | {:error, EmailAlreadyRegisteredError.t() | InvalidDataError.t()}
  def register_user(user_params) do
    with {:ok, user} <- UNAccounts.create_user(user_params) do
      Emails.send_welcome(user.email)
      {:ok, user}
    end
  end

  @spec authenticate_user(map) :: {:ok, User.t()} | {:error, WrongCredentialsError.t()}
  def authenticate_user(%{email: email, password: password}) do
    with {:ok, user} <- UNAccounts.authenticate_by_email_and_pass(email, password) do
      {:ok, user}
    end
  end

  @spec get_user_by_id(String.t()) :: {:ok, User.t()} | {:error, UserNotFoundError.t()}
  defdelegate get_user_by_id(user_id), to: UNAccounts

  @spec initiate_password_reset(String.t()) ::
          {:ok, PasswordReset.t()} | {:error, UserNotFoundError.t()}
  def initiate_password_reset(user_email) do
    with {:ok, %PasswordReset{} = password_reset} <-
           UNAccounts.create_password_reset_code(user_email) do
      Emails.send_password_reset(user_email, password_reset)
      {:ok, password_reset}
    end
  end

  @spec validate_password_reset_code(String.t()) ::
          {:ok, PasswordReset.t()} | {:error, InvalidDataError.t()}
  defdelegate validate_password_reset_code(password_reset_code), to: UNAccounts

  @spec update_user_password(String.t(), map) :: {:ok, User.t()} | {:error, InvalidDataError.t()}
  defdelegate update_user_password(password_reset_code, update), to: UNAccounts
end
