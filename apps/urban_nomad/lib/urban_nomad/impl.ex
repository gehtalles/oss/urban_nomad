defmodule UrbanNomad.Impl do
  @moduledoc false

  alias UrbanNomad.{
    EmailAlreadyRegisteredError,
    InvalidDataError,
    PasswordReset,
    User,
    UserNotFoundError,
    WrongCredentialsError
  }

  @callback register_user(map) ::
              {:ok, User.t()} | {:error, EmailAlreadyRegisteredError.t() | InvalidDataError.t()}

  @callback authenticate_user(map) ::
              {:ok, User.t()} | {:error, WrongCredentialsError.t() | UserNotFoundError.t()}

  @callback get_user_by_id(String.t()) :: {:ok, User.t()} | {:error, UserNotFoundError.t()}

  @callback initiate_password_reset(String.t()) ::
              {:ok, PasswordReset.t()} | {:error, UserNotFoundError.t()}
  @callback validate_password_reset_code(String.t()) ::
              {:ok, PasswordReset.t()} | {:error, InvalidDataError.t()}

  @callback update_user_password(String.t(), map) ::
              {:ok, User.t()} | {:error, InvalidDataError.t()}
end
