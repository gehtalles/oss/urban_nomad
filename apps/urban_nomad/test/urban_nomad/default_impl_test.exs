defmodule UrbanNomad.DefaultImplTest do
  use ExUnit.Case, async: true

  import Mox
  import UrbanNomad.TestHelper

  alias UrbanNomad.{
    DefaultImpl,
    InvalidDataError,
    MockAccounts,
    MockEmails,
    PasswordReset,
    User,
    UserNotFoundError,
    WrongCredentialsError
  }

  setup [:set_mox_from_context, :verify_on_exit!]

  @valid_user_attrs valid_user_attrs()
  @valid_user valid_user_struct()
  @valid_password_reset valid_password_reset_struct()

  describe "register_user/1" do
    test "with valid data" do
      expect(MockAccounts, :create_user, fn _ -> {:ok, @valid_user} end)
      expect(MockEmails, :send_welcome, fn _ -> :ok end)
      assert {:ok, _} = DefaultImpl.register_user(@valid_user_attrs)
    end

    test "with invalid data" do
      expect(MockAccounts, :create_user, fn _ ->
        {:error, InvalidDataError.exception(errors: [])}
      end)

      assert {:error, %InvalidDataError{}} = DefaultImpl.register_user(%{})
    end
  end

  describe "authenticate_user/1" do
    test "with valid data" do
      expect(MockAccounts, :authenticate_by_email_and_pass, fn _, _ ->
        {:ok, @valid_user}
      end)

      assert {:ok, _} = DefaultImpl.authenticate_user(@valid_user_attrs)
    end

    test "with invalid data" do
      params = %{email: "invalid", password: "invalid"}

      expect(MockAccounts, :authenticate_by_email_and_pass, fn _, _ ->
        {:error, WrongCredentialsError.exception(errors: "wrong email")}
      end)

      assert {:error, %WrongCredentialsError{}} = DefaultImpl.authenticate_user(params)
    end
  end

  describe "get_user_by_id/1" do
    test "with valid data" do
      expect(MockAccounts, :get_user_by_id, fn _ -> {:ok, @valid_user} end)
      assert {:ok, _} = DefaultImpl.get_user_by_id("1234")
    end

    test "with invalid data" do
      expect(MockAccounts, :get_user_by_id, fn _ ->
        {:error, UserNotFoundError.exception(error: "not found")}
      end)

      assert {:error, %UserNotFoundError{}} = DefaultImpl.get_user_by_id("1234")
    end
  end

  describe "initiate_password_reset/1" do
    test "with valid data" do
      email = "foo"
      password_reset = @valid_password_reset
      expect(MockAccounts, :create_password_reset_code, fn ^email -> {:ok, password_reset} end)
      expect(MockEmails, :send_password_reset, fn ^email, ^password_reset -> :ok end)

      assert {:ok, %PasswordReset{}} = DefaultImpl.initiate_password_reset(email)
    end

    test "with invalid data" do
      expect(MockAccounts, :create_password_reset_code, fn _ ->
        {:error, UserNotFoundError.exception(error: "not found")}
      end)

      assert {:error, %UserNotFoundError{}} = DefaultImpl.initiate_password_reset("1234")
    end
  end

  describe "validate_password_reset_code/1" do
    test "with valid data" do
      %{code: password_reset_code} = password_reset = @valid_password_reset

      expect(MockAccounts, :validate_password_reset_code, fn ^password_reset_code ->
        {:ok, password_reset}
      end)

      assert {:ok, %PasswordReset{}} =
               DefaultImpl.validate_password_reset_code(password_reset_code)
    end

    test "with invalid data" do
      password_reset_code = "123"

      expect(MockAccounts, :validate_password_reset_code, fn ^password_reset_code ->
        {:error, InvalidDataError.exception(errors: "not found")}
      end)

      assert {:error, %InvalidDataError{}} =
               DefaultImpl.validate_password_reset_code(password_reset_code)
    end
  end

  describe "update_user_password/2" do
    test "with valid data" do
      %{code: password_reset_code} = @valid_password_reset

      update = %{
        password: "123",
        password_confirmation: "123"
      }

      expect(MockAccounts, :update_user_password, fn ^password_reset_code, ^update ->
        {:ok, @valid_user}
      end)

      assert {:ok, %User{}} = DefaultImpl.update_user_password(password_reset_code, update)
    end

    test "with invalid data" do
      password_reset_code = "123"
      update = %{}

      expect(MockAccounts, :update_user_password, fn ^password_reset_code, ^update ->
        {:error, InvalidDataError.exception(errors: "not found")}
      end)

      assert {:error, %InvalidDataError{}} =
               DefaultImpl.update_user_password(password_reset_code, update)
    end
  end
end
