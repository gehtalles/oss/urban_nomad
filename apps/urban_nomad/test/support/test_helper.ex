defmodule UrbanNomad.TestHelper do
  @moduledoc false

  alias Ecto.Changeset
  alias UNAccounts.TestHelper, as: Accounts

  defdelegate valid_user_attrs, to: Accounts
  defdelegate valid_password_reset_attrs, to: Accounts
  def valid_user_struct, do: struct(UrbanNomad.User, valid_user_attrs() |> Map.put(:id, "123"))

  def valid_password_reset_struct,
    do: struct(UrbanNomad.PasswordReset, valid_password_reset_attrs())

  @doc """
  A helper that transform changeset errors to a map of messages.
      assert {:error, changeset} = Accounts.create_user(%{password: "short"})
      assert "password is too short" in errors_on(changeset).password
      assert %{password: ["password is too short"]} = errors_on(changeset)
  """
  def errors_on(changeset) do
    Changeset.traverse_errors(changeset, fn {message, opts} ->
      Enum.reduce(opts, message, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
  end
end
