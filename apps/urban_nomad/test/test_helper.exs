ExUnit.start()

Mox.defmock(UrbanNomad.MockAccounts, for: UNAccounts.Impl)
Application.put_env(:un_accounts, :impl, UrbanNomad.MockAccounts)

Mox.defmock(UrbanNomad.MockEmails, for: UNNotifications.Emails.Impl)
Application.put_env(:un_notifications, :impl, UrbanNomad.MockEmails)
