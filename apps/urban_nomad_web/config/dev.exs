use Mix.Config

config :urban_nomad_web, UrbanNomadWeb.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [
    node: [
      "node_modules/webpack/bin/webpack.js",
      "--mode",
      "development",
      "--watch-stdin",
      cd: Path.expand("../assets", __DIR__)
    ]
  ]

config :urban_nomad_web, UrbanNomadWeb.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{lib/urban_nomad_web/views/.*(ex)$},
      ~r{lib/urban_nomad_web/templates/.*(eex)$}
    ]
  ]
