use Mix.Config

config :urban_nomad_web, UrbanNomadWeb.Endpoint,
  http: [port: 4002],
  server: false
