use Mix.Config

config :urban_nomad_web, UrbanNomadWeb.Endpoint,
  cache_static_manifest: "priv/static/cache_manifest.json",
  server: true
