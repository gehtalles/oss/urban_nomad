use Mix.Config

config :urban_nomad_web, UrbanNomadWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "r+Yfx2BH18S2hwTgxDKQUR34butTiyB+Nrn5I9SyZNnKU8Ihb2ub7nCc+ZOf1AgU",
  render_errors: [view: UrbanNomadWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: UrbanNomadWeb.PubSub, adapter: Phoenix.PubSub.PG2]

config :phoenix, :json_library, Jason

config :urban_nomad_web, UrbanNomadWeb.Session.GuardianImpl.Tokenizer,
  issuer: "urban_nomad_dev",
  secret_key: "JOmyhAfMw2J1firyu/Wh0BNh7zcR23DU6izxdm/CXAUl7X1VgwmN3oP1AtP6Grqw"

import_config "#{Mix.env()}.exs"
