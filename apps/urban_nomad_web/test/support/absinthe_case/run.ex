defmodule UrbanNomadWeb.AbsintheCase.Run do
  @moduledoc false
  import UrbanNomad.TestHelper
  alias UrbanNomadWeb.ApiGraphql.Schema

  @valid_user valid_user_struct()
  def run(document, schema \\ Schema, options \\ []) do
    Absinthe.run(document, schema, options)
  end

  def run_as_user(document, schema \\ Schema, options \\ []) do
    run(document, schema, [context: %{session: @valid_user}] ++ options)
  end
end
