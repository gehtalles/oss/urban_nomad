defmodule UrbanNomadWeb.AbsintheCase do
  @moduledoc false

  defmacro __using__(opts) do
    quote do
      use ExUnit.Case, unquote(opts)

      import UrbanNomadWeb.AbsintheCase.Run
      import UrbanNomadWeb.AbsintheCase.AssertionsResult
    end
  end
end
