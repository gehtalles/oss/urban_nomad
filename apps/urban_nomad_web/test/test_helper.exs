ExUnit.start()

Mox.defmock(UrbanNomadWeb.MockUrbanNomad, for: UrbanNomad.Impl)
Application.put_env(:urban_nomad, :impl, UrbanNomadWeb.MockUrbanNomad)

Mox.defmock(UrbanNomadWeb.MockSession, for: UrbanNomadWeb.Session.Impl)
Application.put_env(:urban_nomad_web_session, :impl, UrbanNomadWeb.MockSession)
