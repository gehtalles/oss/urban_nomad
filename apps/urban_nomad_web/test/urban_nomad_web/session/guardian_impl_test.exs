defmodule UrbanNomadWeb.Session.GuadianImplTest do
  use UrbanNomadWeb.ConnCase, async: true

  import UrbanNomad.TestHelper

  alias UrbanNomadWeb.Session.GuardianImpl, as: Session

  @valid_user valid_user_struct()
  describe "sign_in/2" do
    test "with valid params", %{conn: conn} do
      assert conn
             |> Session.sign_in(@valid_user)
             |> Session.authenticated?()
    end

    test "with invalid params", %{conn: conn} do
      assert {:error, _} = conn |> Session.sign_in(%{})
    end
  end

  describe "sign_out/2" do
    test "with session", %{conn: conn} do
      refute conn
             |> Session.sign_in(@valid_user)
             |> Session.sign_out()
             |> Session.authenticated?()
    end
  end

  describe "current_token/1" do
    test "with session", %{conn: conn} do
      assert conn
             |> Session.sign_in(@valid_user)
             |> Session.current_token()
             |> is_binary()
    end

    test "without session", %{conn: conn} do
      refute conn |> Session.current_token()
    end
  end
end
