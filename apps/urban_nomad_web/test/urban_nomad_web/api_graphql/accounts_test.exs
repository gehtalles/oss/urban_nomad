defmodule UrbanNomadWeb.ApiGraphql.AccountsTest do
  use UrbanNomadWeb.AbsintheCase, async: true

  import UrbanNomad.TestHelper
  import Mox

  alias UrbanNomad.UserNotFoundError

  alias UrbanNomadWeb.{
    MockUrbanNomad
  }

  describe "query me" do
    @query """
    query {
      me {
        display_name
        email
      }
    }
    """
    test "with an existing user" do
      user = valid_user_struct()

      MockUrbanNomad
      |> expect(:get_user_by_id, fn _ -> {:ok, user} end)

      %{
        "me" => %{
          "email" => user.email,
          "display_name" => user.display_name
        }
      }
      |> assert_data(run_as_user(@query))
    end

    test "with an non existing user" do
      MockUrbanNomad
      |> expect(:get_user_by_id, fn _ ->
        {:error, UserNotFoundError.exception(error: "not found")}
      end)

      ~s(not found)
      |> assert_error_message(run_as_user(@query))
    end
  end
end
