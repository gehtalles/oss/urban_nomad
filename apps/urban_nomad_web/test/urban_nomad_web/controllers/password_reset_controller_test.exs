defmodule UrbanNomadWeb.PasswordResetControllerTest do
  use UrbanNomadWeb.ConnCase, async: true

  import Mox
  import UrbanNomad.TestHelper

  alias UrbanNomad.InvalidDataError

  alias UrbanNomadWeb.{
    MockSession,
    MockUrbanNomad
  }

  setup [:set_mox_from_context, :verify_on_exit!]

  describe "new/2" do
    test "GET renders form", %{conn: conn} do
      expect(MockSession, :authenticated?, 2, fn _ -> false end)
      conn = get(conn, Routes.password_reset_path(conn, :new))
      assert html_response(conn, 200) =~ "Reset your password!"
    end
  end

  describe "create/2" do
    test "POST with valid params creates a password reset", %{conn: conn} do
      expect(MockSession, :authenticated?, fn _conn -> false end)

      conn =
        conn
        |> bypass_through(UrbanNomadWeb.Router, :browser)
        |> get("/")

      email = valid_user_attrs().email

      expect(MockUrbanNomad, :initiate_password_reset, fn ^email ->
        {:ok, valid_password_reset_struct()}
      end)

      post_conn =
        post(conn, Routes.password_reset_path(conn, :create), password_reset: %{email: email})

      assert get_flash(post_conn, :info) =~ "send"
    end

    test "POST rerenders form", %{conn: conn} do
      expect(MockSession, :authenticated?, 2, fn _conn -> false end)
      params = %{}

      conn = post(conn, Routes.password_reset_path(conn, :create), password_reset: params)
      assert html_response(conn, 200) =~ "Reset your password!"
    end
  end

  describe "show/2" do
    test "ensure password_reset code is valid", %{conn: conn} do
      expect(MockSession, :authenticated?, 1, fn _conn -> false end)

      %{code: password_reset_code} = password_reset = valid_password_reset_struct()

      expect(MockUrbanNomad, :validate_password_reset_code, fn ^password_reset_code ->
        {:ok, password_reset}
      end)

      conn = get(conn, Routes.password_reset_path(conn, :show, password_reset_code))
      assert html_response(conn, 200) =~ "Reset your password!"
    end

    test "if invalid password_reset code render 404", %{conn: conn} do
      expect(MockSession, :authenticated?, 1, fn _conn -> false end)

      %{code: password_reset_code} = valid_password_reset_struct()

      expect(MockUrbanNomad, :validate_password_reset_code, fn ^password_reset_code ->
        {:error, InvalidDataError.exception(errors: "Code not found")}
      end)

      conn = get(conn, Routes.password_reset_path(conn, :show, password_reset_code))
      assert html_response(conn, 404) =~ "Not Found"
    end
  end

  describe "update/2" do
    test "with valid data", %{conn: conn} do
      %{code: password_reset_code} = password_reset = valid_password_reset_struct()

      expect(MockUrbanNomad, :validate_password_reset_code, fn ^password_reset_code ->
        {:ok, password_reset}
      end)

      new_password = "123abc456"

      params = %{
        password: new_password,
        password_confirmation: new_password
      }

      expect(MockUrbanNomad, :update_user_password, fn ^password_reset_code,
                                                       %{password: ^new_password} ->
        {:ok, valid_user_struct()}
      end)

      conn =
        post(conn, Routes.password_reset_path(conn, :update, password_reset_code),
          update_password: params
        )

      assert redirected_to(conn) == Routes.session_path(conn, :new)
    end
  end
end
