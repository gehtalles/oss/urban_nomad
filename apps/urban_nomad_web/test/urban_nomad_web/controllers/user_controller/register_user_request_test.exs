defmodule UrbanNomadWeb.UserController.RegisterUserRequestTest do
  use ExUnit.Case, async: true
  use ExUnitProperties

  import Mox
  import UrbanNomad.TestHelper
  alias Ecto.Changeset
  alias UrbanNomad.{EmailAlreadyRegisteredError, User}
  alias UrbanNomadWeb.MockUrbanNomad
  alias UrbanNomadWeb.UserController.RegisterUserRequest

  setup [:set_mox_from_context, :verify_on_exit!]

  describe "run/1" do
    test "run/1 calls register_user with the correct params" do
      check all params <-
                  fixed_map(%{
                    "email" => string(:alphanumeric, min_length: 1),
                    "password" => string(:alphanumeric, min_length: 8),
                    "display_name" => string(:alphanumeric, min_length: 8)
                  }) do
        user = %User{email: params["email"]}

        MockUrbanNomad
        |> expect(:register_user, fn _ -> {:ok, user} end)

        assert {:ok, ^user} = RegisterUserRequest.run(params)
      end
    end

    test "run/1 with missing params" do
      params = %{}

      assert {:error, %Changeset{} = changeset} = RegisterUserRequest.run(params)
      assert "can't be blank" in errors_on(changeset).email
      assert "can't be blank" in errors_on(changeset).password
    end

    test "run/1 with too short of a password" do
      {:error, changeset} = RegisterUserRequest.run(%{password: "a"})
      assert "should be at least 8 character(s)" in errors_on(changeset).password
    end

    test "run/1 when register_user returns an error" do
      params = valid_user_attrs()

      MockUrbanNomad
      |> expect(:register_user, fn _ ->
        {:error, EmailAlreadyRegisteredError.exception(email: params.email)}
      end)

      assert {:error, %Changeset{data: %RegisterUserRequest{}, action: :insert} = changeset} =
               RegisterUserRequest.run(params)

      assert "is already taken" in errors_on(changeset).email
    end
  end
end
