defmodule UrbanNomadWeb.PageControllerTest do
  use UrbanNomadWeb.ConnCase, async: true

  import Mox

  alias UrbanNomadWeb.{MockSession}

  setup [:set_mox_from_context, :verify_on_exit!]

  test "GET /", %{conn: conn} do
    expect(MockSession, :authenticated?, fn _ -> false end)
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Welcome to"
  end
end
