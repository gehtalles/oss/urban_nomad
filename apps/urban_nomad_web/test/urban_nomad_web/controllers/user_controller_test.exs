defmodule UrbanNomadWeb.UserControllerTest do
  use UrbanNomadWeb.ConnCase, async: true

  import Mox
  import UrbanNomad.TestHelper

  alias UrbanNomadWeb.{
    MockSession,
    MockUrbanNomad
  }

  setup [:set_mox_from_context, :verify_on_exit!]
  @valid_user_attrs valid_user_attrs()
  @valid_user valid_user_struct()

  describe "new/2" do
    test "GET renders form", %{conn: conn} do
      expect(MockSession, :authenticated?, 2, fn _ -> false end)
      conn = get(conn, Routes.user_path(conn, :new))
      assert html_response(conn, 200) =~ "Join"
    end
  end

  describe "create/2" do
    test "POST with valid params registers a user", %{conn: conn} do
      conn =
        conn
        |> bypass_through(UrbanNomadWeb.Router, :browser)
        |> get("/")

      expect(MockSession, :sign_in, fn _conn, _user ->
        conn
      end)

      expect(MockSession, :authenticated?, fn _ -> false end)

      expect(MockUrbanNomad, :register_user, fn _ ->
        {:ok, @valid_user}
      end)

      post_conn = post(conn, Routes.user_path(conn, :create), user: @valid_user_attrs)

      assert redirected_to(post_conn) == Routes.page_path(conn, :index)
    end

    test "POST create/2 rerenders form", %{conn: conn} do
      expect(MockSession, :authenticated?, 2, fn _ ->
        false
      end)

      params = %{}

      conn = post(conn, Routes.user_path(conn, :create), user: params)
      assert html_response(conn, 200) =~ "Join"
    end
  end
end
