defmodule UrbanNomadWeb.SessionControllerTest do
  use UrbanNomadWeb.ConnCase, async: true

  import Mox
  import UrbanNomad.TestHelper

  alias UrbanNomadWeb.{
    MockSession,
    MockUrbanNomad
  }

  setup [:set_mox_from_context, :verify_on_exit!]

  @valid_user_attrs valid_user_attrs()
  @valid_user valid_user_struct()

  describe "new/2" do
    test "GET renders form", %{conn: conn} do
      expect(MockSession, :authenticated?, 2, fn _ -> false end)
      conn = get(conn, Routes.session_path(conn, :new))
      assert html_response(conn, 200) =~ "Welcome Back!"
    end
  end

  describe "create/2" do
    test "POST with valid params authenticates a user", %{conn: conn} do
      conn =
        conn
        |> bypass_through(UrbanNomadWeb.Router, :browser)
        |> get("/")

      user = @valid_user
      user_attrs = @valid_user_attrs

      params = %{
        email: user_attrs.email,
        password: user_attrs.password
      }

      expect(MockSession, :authenticated?, fn _conn -> true end)

      expect(MockUrbanNomad, :authenticate_user, fn _signin_user_request ->
        {:ok, user}
      end)

      expect(MockSession, :sign_in, fn _conn, _user ->
        conn
      end)

      post_conn = post(conn, Routes.session_path(conn, :create), session: params)
      assert redirected_to(post_conn) == Routes.page_path(conn, :index)
    end

    test "POST rerenders form", %{conn: conn} do
      expect(MockSession, :authenticated?, 2, fn _conn -> false end)
      params = %{}

      conn = post(conn, Routes.session_path(conn, :create), session: params)
      assert html_response(conn, 200) =~ "Welcome Back!"
    end
  end

  describe "destroy/2" do
    test "GET destroys the current session", %{conn: conn} do
      conn =
        conn
        |> bypass_through(UrbanNomadWeb.Router, :browser)
        |> get("/")

      expect(MockSession, :sign_out, fn _ ->
        conn
      end)

      get_conn = get(conn, Routes.session_path(conn, :destroy))
      assert redirected_to(get_conn) == Routes.session_path(conn, :new)
    end
  end
end
