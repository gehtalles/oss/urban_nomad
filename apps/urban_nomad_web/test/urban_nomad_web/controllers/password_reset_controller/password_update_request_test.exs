defmodule UrbanNomadWeb.PasswordResetController.PasswordUpdateRequestTest do
  use ExUnit.Case, async: true
  use ExUnitProperties

  import Mox
  import UrbanNomad.TestHelper

  alias Ecto.Changeset
  alias UrbanNomad.{User}

  alias UrbanNomadWeb.{
    MockUrbanNomad,
    PasswordResetController.PasswordUpdateRequest
  }

  setup [:set_mox_from_context, :verify_on_exit!]

  describe "run/1" do
    test "calls update_user_password with the correct params" do
      check all params <-
                  fixed_map(%{
                    "password" => string(:alphanumeric, min_length: 8),
                    "password_reset_code" => string(:alphanumeric, min_length: 13)
                  }) do
        %{
          "password_reset_code" => password_reset_code,
          "password" => password
        } = params

        user = valid_user_struct()

        params = Map.put(params, "password_confirmation", password)

        MockUrbanNomad
        |> expect(:update_user_password, fn ^password_reset_code, %{password: ^password} ->
          {:ok, user}
        end)

        assert {:ok, %User{} = ^user} = PasswordUpdateRequest.run(params)
      end
    end

    test "with invalid params" do
      params = %{}

      assert {:error, %Changeset{action: :insert} = changeset} = PasswordUpdateRequest.run(params)
      assert "can't be blank" in errors_on(changeset).password
      assert "can't be blank" in errors_on(changeset).password_confirmation
      assert "does not match password" in errors_on(changeset).password_confirmation
    end
  end
end
