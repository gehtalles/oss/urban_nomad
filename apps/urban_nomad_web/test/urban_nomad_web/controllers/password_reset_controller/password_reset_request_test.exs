defmodule UrbanNomadWeb.PasswordResetController.PasswordResetRequestTest do
  use ExUnit.Case, async: true
  use ExUnitProperties

  import Mox
  import UrbanNomad.TestHelper

  alias Ecto.Changeset
  alias UrbanNomad.{UserNotFoundError}

  alias UrbanNomadWeb.{
    MockUrbanNomad,
    PasswordResetController.PasswordResetRequest
  }

  setup [:set_mox_from_context, :verify_on_exit!]

  describe "run/1" do
    test "calls initiate_password_reset with the correct params" do
      check all params <-
                  fixed_map(%{
                    "email" => string(:alphanumeric, min_length: 1)
                  }) do
        password_reset = valid_password_reset_struct()
        email = params["email"]

        MockUrbanNomad
        |> expect(:initiate_password_reset, fn ^email -> {:ok, password_reset} end)

        assert {:ok, ^password_reset} = PasswordResetRequest.run(params)
      end
    end

    test "with missing params" do
      params = %{}

      assert {:error, %Changeset{} = changeset} = PasswordResetRequest.run(params)
      assert "can't be blank" in errors_on(changeset).email
    end

    test "when initiate_password_reset returns an error" do
      params = valid_user_attrs()

      MockUrbanNomad
      |> expect(:initiate_password_reset, fn _ ->
        {:error, UserNotFoundError.exception(error: "email not found")}
      end)

      assert {:error, %Changeset{data: %PasswordResetRequest{}, action: :insert} = changeset} =
               PasswordResetRequest.run(params)

      assert "email not found" in errors_on(changeset).email
    end
  end
end
