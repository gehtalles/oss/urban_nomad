defmodule UrbanNomadWeb.SessionController.SigninUserRequestTest do
  use ExUnit.Case, async: true
  use ExUnitProperties

  import Mox
  import UrbanNomad.TestHelper

  alias Ecto.Changeset
  alias UrbanNomad.{User}

  alias UrbanNomadWeb.{
    MockUrbanNomad,
    SessionController.SigninUserRequest
  }

  setup [:set_mox_from_context, :verify_on_exit!]

  describe "run/1" do
    test "calls authenticate_user with the correct params" do
      check all params <-
                  fixed_map(%{
                    "email" => string(:alphanumeric, min_length: 1),
                    "password" => string(:alphanumeric, min_length: 8)
                  }) do
        user = %User{email: params["email"]}

        MockUrbanNomad
        |> expect(:authenticate_user, fn _ -> {:ok, user} end)

        assert {:ok, ^user} = SigninUserRequest.run(params)
      end
    end

    test "with missing params" do
      params = %{}

      assert {:error, %Changeset{action: :insert} = changeset} = SigninUserRequest.run(params)

      assert "can't be blank" in errors_on(changeset).email
      assert "can't be blank" in errors_on(changeset).password
    end
  end
end
