defmodule UrbanNomadWeb.SPAController do
  @moduledoc false
  use UrbanNomadWeb, :controller
  alias UrbanNomadWeb.Session

  plug(:redirect_if_authenticated)

  def index(conn, _params) do
    token = conn |> Session.current_token()

    conn
    |> assign(:token, token)
    |> render("index.html")
  end

  defp redirect_if_authenticated(conn, _opts) do
    if Session.authenticated?(conn) do
      conn
    else
      conn
      |> redirect(to: Routes.page_path(conn, :index))
    end
  end
end
