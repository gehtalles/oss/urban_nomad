defmodule UrbanNomadWeb.SessionController.SigninUserRequest do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  alias Ecto.Changeset
  alias UrbanNomad.{User, WrongCredentialsError}

  @type t :: %__MODULE__{
          email: String.t() | nil,
          password: String.t() | nil
        }

  @primary_key false
  embedded_schema do
    field(:email, :string)
    field(:password, :string)
  end

  @spec run(map) :: {:ok, User.t()} | {:error, Changeset.t()}
  def run(params) do
    changeset = changeset(%__MODULE__{}, params)

    with {:ok, request} <- apply_action(changeset, :insert) do
      authenticate_user(request, changeset)
    end
  end

  @cast_fields ~w(email password)a
  @required_fields ~w(email password)a
  @spec changeset(t, map()) :: Changeset.t()
  def changeset(request \\ %__MODULE__{}, params \\ %{}) do
    request
    |> cast(params, @cast_fields)
    |> validate_required(@required_fields)
  end

  @spec authenticate_user(t, Changeset.t()) :: {:ok, User.t()} | {:error, Changeset.t()}
  defp authenticate_user(%__MODULE__{} = user_params, changeset) do
    case user_params |> Map.from_struct() |> UrbanNomad.authenticate_user() do
      {:ok, %User{} = user} ->
        {:ok, user}

      {:error, %WrongCredentialsError{errors: _}} ->
        {:error, changeset |> set_changeset_action(:insert)}
    end
  end

  defp set_changeset_action(changeset, action), do: %Changeset{changeset | action: action}
end
