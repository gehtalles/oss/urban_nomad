defmodule UrbanNomadWeb.UserController do
  @moduledoc false
  use UrbanNomadWeb, :controller

  alias UrbanNomad.User

  alias UrbanNomadWeb.{
    Session,
    UserController.RegisterUserRequest
  }

  alias UrbanNomadWeb.Session

  plug(:redirect_if_authenticated when action in [:new, :create])

  def new(conn, _params) do
    changeset = RegisterUserRequest.changeset()
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- RegisterUserRequest.run(user_params),
         conn <- conn |> Session.sign_in(user) do
      conn
      |> put_flash(:info, "User created successfully.")
      |> redirect(to: Routes.page_path(conn, :index))
    else
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  defp redirect_if_authenticated(conn, _opts) do
    if Session.authenticated?(conn) do
      conn
      |> redirect(to: Routes.page_path(conn, :index))
    else
      conn
    end
  end
end
