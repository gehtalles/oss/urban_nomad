defmodule UrbanNomadWeb.SessionController do
  @moduledoc false
  use UrbanNomadWeb, :controller

  alias UrbanNomad.User

  alias UrbanNomadWeb.{
    Session,
    SessionController.SigninUserRequest
  }

  plug(:redirect_if_authenticated when action in [:new, :create])

  def new(conn, _params) do
    changeset = SigninUserRequest.changeset()
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"session" => session_params}) do
    with {:ok, %User{} = user} <- SigninUserRequest.run(session_params),
         conn <- conn |> Session.sign_in(user) do
      conn
      |> put_flash(:info, "User loggin successfully.")
      |> redirect(to: Routes.page_path(conn, :index))
    else
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def destroy(conn, _) do
    conn
    |> Session.sign_out()
    |> put_flash(:info, "User loggout successfully.")
    |> redirect(to: Routes.session_path(conn, :new))
  end

  defp redirect_if_authenticated(conn, _opts) do
    if Session.authenticated?(conn) do
      conn
      |> redirect(to: Routes.page_path(conn, :index))
    else
      conn
    end
  end
end
