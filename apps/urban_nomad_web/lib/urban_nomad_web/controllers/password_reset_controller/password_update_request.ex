defmodule UrbanNomadWeb.PasswordResetController.PasswordUpdateRequest do
  @moduledoc false

  use Ecto.Schema

  import Ecto.Changeset

  alias Ecto.Changeset

  alias UrbanNomad.{
    InvalidDataError,
    User
  }

  @type t :: %__MODULE__{
          password: String.t() | nil,
          password_confirmation: String.t() | nil
        }

  @primary_key false
  embedded_schema do
    field(:password_reset_code, :binary_id)
    field(:password, :string)
    field(:password_confirmation, :string)
  end

  @spec run(map) :: {:ok, User.t()} | {:error, Changeset.t()}
  def run(params) do
    changeset = changeset(%__MODULE__{}, params)

    with {:ok, request} <- apply_action(changeset, :insert) do
      update_password(request, changeset)
    end
  end

  @cast_fields ~w(password_reset_code password password_confirmation)a
  @required_fields ~w(password_reset_code password password_confirmation)a
  @spec changeset(t, map()) :: Changeset.t()
  def changeset(request \\ %__MODULE__{}, params \\ %{}) do
    request
    |> cast(params, @cast_fields)
    |> validate_required(@required_fields)
    |> validate_length(:password, min: 8)
    |> validate_confirmation(:password, message: "does not match password", required: true)
  end

  @spec update_password(t, Changeset.t()) :: {:ok, User.t()} | {:error, Changeset.t()}
  defp update_password(%__MODULE__{} = update_params, changeset) do
    %{password_reset_code: password_reset_code, password: password} = update_params

    case UrbanNomad.update_user_password(password_reset_code, %{password: password}) do
      {:ok, %User{} = user} -> {:ok, user}
      {:error, %InvalidDataError{}} -> {:error, changeset |> set_changeset_action(:insert)}
    end
  end

  defp set_changeset_action(changeset, action), do: %Changeset{changeset | action: action}
end
