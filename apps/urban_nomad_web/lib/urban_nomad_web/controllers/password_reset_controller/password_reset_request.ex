defmodule UrbanNomadWeb.PasswordResetController.PasswordResetRequest do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  alias Ecto.Changeset

  alias UrbanNomad.{
    PasswordReset,
    UserNotFoundError
  }

  @type t :: %__MODULE__{email: String.t() | nil}

  @primary_key false
  embedded_schema do
    field(:email, :string)
  end

  @spec run(map) :: {:ok, PasswordReset.t()} | {:error, Changeset.t()}
  def run(params) do
    changeset = changeset(%__MODULE__{}, params)

    with {:ok, request} <- apply_action(changeset, :insert) do
      initiate_password_reset(request, changeset)
    end
  end

  @cast_fields ~w(email)a
  @required_fields ~w(email)a
  @spec changeset(t, map()) :: Changeset.t()
  def changeset(request \\ %__MODULE__{}, params \\ %{}) do
    request
    |> cast(params, @cast_fields)
    |> validate_required(@required_fields)
  end

  @spec initiate_password_reset(t, Changeset.t()) ::
          {:ok, PasswordReset.t()} | {:error, Changeset.t()}
  defp initiate_password_reset(%__MODULE__{email: email}, changeset) do
    case UrbanNomad.initiate_password_reset(email) do
      {:ok, %PasswordReset{} = password_reset} ->
        {:ok, password_reset}

      {:error, %UserNotFoundError{error: error}} ->
        changeset =
          changeset
          |> add_error(:email, error)
          |> set_changeset_action(:insert)

        {:error, changeset}
    end
  end

  defp set_changeset_action(changeset, action), do: %Changeset{changeset | action: action}
end
