defmodule UrbanNomadWeb.PasswordResetController do
  @moduledoc false

  use UrbanNomadWeb, :controller

  alias UrbanNomad.{
    PasswordReset,
    User
  }

  alias UrbanNomadWeb.{
    ErrorView,
    PasswordResetController.PasswordResetRequest,
    PasswordResetController.PasswordUpdateRequest,
    Session
  }

  plug(:redirect_if_authenticated when action in [:new, :create])
  plug(:validate_password_reset_code when action in [:show, :update])

  def new(conn, _params) do
    changeset = PasswordResetRequest.changeset()
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"password_reset" => password_reset_params}) do
    with {:ok, %PasswordReset{}} <- PasswordResetRequest.run(password_reset_params) do
      conn
      |> put_flash(:info, "Password Reset Instruction send.")
      |> redirect(to: Routes.password_reset_path(conn, :new))
    else
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, _params) do
    changeset = PasswordUpdateRequest.changeset()
    render(conn, "show.html", changeset: changeset)
  end

  def update(conn, %{"update_password" => update_password_params}) do
    # XXX-jt-151118 enhance params with password_reset_code
    password_reset_code = conn.assigns.password_reset.code
    params = update_password_params |> Map.put("password_reset_code", password_reset_code)

    case PasswordUpdateRequest.run(params) do
      {:ok, %User{}} ->
        conn
        |> put_flash(:info, "Password updated.")
        |> redirect(to: Routes.session_path(conn, :new))

      {:error, changeset} ->
        render(conn, "show.html", changeset: changeset)
    end
  end

  defp redirect_if_authenticated(conn, _opts) do
    if Session.authenticated?(conn) do
      conn
      |> redirect(to: Routes.page_path(conn, :index))
    else
      conn
    end
  end

  defp validate_password_reset_code(conn, _opts) do
    code = conn.params["id"]

    case UrbanNomad.validate_password_reset_code(code) do
      {:ok, password_reset} ->
        conn |> assign(:password_reset, password_reset)

      _ ->
        conn
        |> put_status(404)
        |> put_view(ErrorView)
        |> render("404.html")
        |> halt()
    end
  end
end
