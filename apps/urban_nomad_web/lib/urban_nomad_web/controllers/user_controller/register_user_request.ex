defmodule UrbanNomadWeb.UserController.RegisterUserRequest do
  @moduledoc false
  use Ecto.Schema
  import Ecto.Changeset

  alias Ecto.Changeset
  alias UrbanNomad.{EmailAlreadyRegisteredError, User}

  @type t :: %__MODULE__{
          email: String.t() | nil,
          password: String.t() | nil,
          display_name: String.t() | nil
        }

  @primary_key false
  embedded_schema do
    field(:email, :string)
    field(:password, :string)
    field(:display_name, :string)
  end

  @spec run(map) :: {:ok, User.t()} | {:error, Changeset.t()}
  def run(params) do
    changeset = changeset(%__MODULE__{}, params)

    with {:ok, request} <- apply_action(changeset, :insert) do
      register_user(request, changeset)
    end
  end

  @cast_fields ~w(email password display_name)a
  @required_fields ~w(email password display_name)a
  @spec changeset(t, map()) :: Changeset.t()
  def changeset(request \\ %__MODULE__{}, params \\ %{}) do
    request
    |> cast(params, @cast_fields)
    |> validate_required(@required_fields)
    |> validate_length(:password, min: 8)
  end

  @spec register_user(t, Changeset.t()) :: {:ok, User.t()} | {:error, Changeset.t()}
  defp register_user(%__MODULE__{} = user_params, changeset) do
    case user_params |> Map.from_struct() |> UrbanNomad.register_user() do
      {:ok, %User{} = user} ->
        {:ok, user}

      {:error, %EmailAlreadyRegisteredError{}} ->
        changeset =
          changeset
          |> add_error(:email, "is already taken")
          |> set_changeset_action(:insert)

        {:error, changeset}
    end
  end

  defp set_changeset_action(changeset, action), do: %Changeset{changeset | action: action}
end
