defmodule UrbanNomadWeb.Application do
  @moduledoc false

  use Application

  alias UrbanNomadWeb.Endpoint

  def start(_type, _args) do
    children = [
      Endpoint
    ]

    opts = [strategy: :one_for_one, name: UrbanNomadWeb.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    Endpoint.config_change(changed, removed)
    :ok
  end
end
