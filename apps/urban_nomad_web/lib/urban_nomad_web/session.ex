defmodule UrbanNomadWeb.Session do
  @moduledoc """
  Session Module
  """
  @behaviour UrbanNomadWeb.Session.Impl

  alias UrbanNomad.User
  alias UrbanNomadWeb.Session.GuardianImpl

  @spec sign_in(Plug.Conn.t(), User.t()) :: Plug.Conn.t() | {:error, :unknown}
  def sign_in(conn, resource), do: impl().sign_in(conn, resource)

  @spec sign_out(Plug.Conn.t()) :: Plug.Conn.t() | nil
  def sign_out(conn), do: impl().sign_out(conn)

  @spec current_token(Plug.Conn.t()) :: String.t() | nil
  def current_token(conn), do: impl().current_token(conn)

  @spec authenticated?(Plug.Conn.t()) :: true | false
  def authenticated?(conn), do: impl().authenticated?(conn)

  defp impl do
    Application.get_env(:urban_nomad_web_session, :impl, GuardianImpl)
  end
end
