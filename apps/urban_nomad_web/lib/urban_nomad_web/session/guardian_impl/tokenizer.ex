defmodule UrbanNomadWeb.Session.GuardianImpl.Tokenizer do
  @moduledoc """
  Guardian Module. Defines subject_for_token/2 and resource_from_claims/1
  """
  use Guardian, otp_app: :urban_nomad_web

  alias UrbanNomad.User

  @unknown_resource_error {:error, "Unknown resource"}

  def subject_for_token(%User{id: id}, _claims), do: {:ok, "u:" <> to_string(id)}

  def subject_for_token(_resource, _claims), do: @unknown_resource_error

  def resource_from_claims(%{"sub" => "u:" <> id}), do: UrbanNomad.get_user_by_id(id)

  def resource_from_claims(_claims), do: @unknown_resource_error
end
