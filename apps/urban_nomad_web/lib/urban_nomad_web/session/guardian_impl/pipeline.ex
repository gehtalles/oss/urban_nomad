defmodule UrbanNomadWeb.Session.GuardianImpl.Pipeline do
  @moduledoc """
  Guardian Pipeline
  """
  @claims %{"typ" => "access"}

  use Guardian.Plug.Pipeline,
    otp_app: :urban_nomad_web,
    error_handler: UrbanNomadWeb.Session.GuardianImpl.ErrorHandler,
    module: UrbanNomadWeb.Session.GuardianImpl.Tokenizer

  plug(Guardian.Plug.VerifySession)
  plug(Guardian.Plug.VerifyHeader, claims: @claims)
  plug(Guardian.Plug.LoadResource, allow_blank: true)
end
