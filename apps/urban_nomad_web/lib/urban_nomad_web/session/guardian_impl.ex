defmodule UrbanNomadWeb.Session.GuardianImpl do
  @moduledoc false

  @behaviour UrbanNomadWeb.Session.Impl

  alias __MODULE__.{Tokenizer}
  alias UrbanNomad.User

  @spec sign_in(Plug.Conn.t(), User.t()) :: Plug.Conn.t() | {:error, :unknown}
  def sign_in(conn, %User{} = resource), do: Tokenizer.Plug.sign_in(conn, resource)

  def sign_in(_, _), do: {:error, :unknown}

  @spec sign_out(Plug.Conn.t()) :: Plug.Conn.t() | nil
  def sign_out(conn), do: Tokenizer.Plug.sign_out(conn)

  @spec current_token(Plug.Conn.t()) :: String.t() | nil
  def current_token(conn), do: Tokenizer.Plug.current_token(conn)

  @spec authenticated?(Plug.Conn.t()) :: true | false
  def authenticated?(conn), do: Tokenizer.Plug.authenticated?(conn)
end
