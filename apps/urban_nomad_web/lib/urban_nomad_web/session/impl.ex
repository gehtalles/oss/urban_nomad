defmodule UrbanNomadWeb.Session.Impl do
  @moduledoc false
  alias UrbanNomad.User

  @callback sign_in(Plug.Conn.t(), User.t()) :: Plug.Conn.t() | {:error, :unknown}

  @callback sign_out(Plug.Conn.t()) :: Plug.Conn.t() | nil

  @callback current_token(Plug.Conn.t()) :: String.t() | nil

  @callback authenticated?(Plug.Conn.t()) :: true | false
end
