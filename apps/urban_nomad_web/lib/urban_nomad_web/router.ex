defmodule UrbanNomadWeb.Router do
  use UrbanNomadWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :session do
    plug(UrbanNomadWeb.Session.GuardianImpl.Pipeline)
  end

  pipeline :graphql do
    plug(
      Plug.Parsers,
      parsers: [:urlencoded, :multipart, :json, Absinthe.Plug.Parser],
      pass: ["*/*"],
      json_decoder: Jason
    )

    plug(:put_secure_browser_headers)
    plug(:accepts, ["json"])
    plug(UrbanNomadWeb.ApiGraphql.Context)
  end

  pipeline :app do
    plug(:put_layout, {UrbanNomadWeb.LayoutView, "spa.html"})
  end

  scope "/", UrbanNomadWeb do
    pipe_through([:browser, :session])

    get("/", PageController, :index)

    get("/signup", UserController, :new)
    post("/signup", UserController, :create)

    get("/signin", SessionController, :new)
    post("/signin", SessionController, :create)
    get("/signout", SessionController, :destroy)

    get("/reset-password", PasswordResetController, :new)
    post("/reset-password", PasswordResetController, :create)

    get("/reset-password/:id", PasswordResetController, :show)
    post("/reset-password/:id", PasswordResetController, :update)
  end

  scope "/app", UrbanNomadWeb do
    pipe_through([:browser, :session, :app])
    get("/*path", SPAController, :index)
  end

  scope "/api" do
    pipe_through([:session, :graphql])

    if Mix.env() == :dev do
      forward(
        "/graphiql",
        Absinthe.Plug.GraphiQL,
        schema: UrbanNomadWeb.ApiGraphql.Schema
      )
    end

    forward(
      "/",
      Absinthe.Plug,
      schema: UrbanNomadWeb.ApiGraphql.Schema
    )
  end
end
