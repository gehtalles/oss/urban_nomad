defmodule UrbanNomadWeb.ApiGraphql.Context do
  @moduledoc """
  Context Plug. Utilizes Guardian.Plug to get current_resource and
  build_context creates a context for Absinthe.
  """

  @behaviour Plug

  import Plug.Conn

  alias Guardian.Plug
  alias UrbanNomad.User

  @impl true
  def init(opts), do: opts

  @impl true
  def call(conn, _) do
    conn
    |> Plug.current_resource()
    # |> IO.inspect(label: "build_context")
    |> build_context(conn)

    # |> IO.inspect(label: "build_context")
  end

  defp build_context(%User{} = user, conn) do
    put_private(conn, :absinthe, %{context: %{session: user}})
  end

  defp build_context(nil, conn), do: conn
end
