defmodule UrbanNomadWeb.ApiGraphql.Accounts.Schema do
  @moduledoc false

  use Absinthe.Schema.Notation

  import_types(UrbanNomadWeb.ApiGraphql.Accounts.Types)
  import_types(UrbanNomadWeb.ApiGraphql.Accounts.Query)
end
