defmodule UrbanNomadWeb.ApiGraphql.Accounts.Query do
  @moduledoc """
  Accounts Query
  """
  use Absinthe.Schema.Notation
  alias UrbanNomadWeb.ApiGraphql.Accounts.Resolver

  object :accounts_query do
    @desc "Get the user account of the currently logged in user."
    field :me, :user do
      resolve(&Resolver.me/2)
    end
  end
end
