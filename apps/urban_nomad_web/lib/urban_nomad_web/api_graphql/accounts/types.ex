defmodule UrbanNomadWeb.ApiGraphql.Accounts.Types do
  @moduledoc false
  use Absinthe.Schema.Notation

  object :user do
    field(:email, non_null(:string))
    field(:display_name, non_null(:string))
  end
end
