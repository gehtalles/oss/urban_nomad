defmodule UrbanNomadWeb.ApiGraphql.Accounts.Resolver do
  @moduledoc false

  def me(_, %{context: %{session: session}}), do: UrbanNomad.get_user_by_id(session.id)
end
