defmodule UrbanNomadWeb.ApiGraphql.Middleware.TransformErrors do
  @moduledoc """
  TODO
  """
  @behaviour Absinthe.Middleware
  alias UrbanNomad.{
    UserNotFoundError
  }

  def call(%{errors: errors} = resolution, _) do
    %{resolution | errors: Enum.flat_map(errors, &handle_error/1)}
  end

  defp handle_error(%UserNotFoundError{error: error}), do: [%{message: error}]

  defp handle_error(error), do: [error]
end
