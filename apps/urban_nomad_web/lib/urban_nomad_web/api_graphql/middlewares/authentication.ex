defmodule UrbanNomadWeb.ApiGraphql.Middleware.Authentication do
  @moduledoc """
  Absinthe Middleware to ensure a session is within the resolution context.
  """
  @behaviour Absinthe.Middleware

  alias Absinthe.Resolution

  def call(%{context: %{session: _}} = resolution, _config), do: resolution

  def call(resolution, _config),
    do: resolution |> Resolution.put_result({:error, "unauthenticated"})
end
