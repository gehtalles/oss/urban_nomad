defmodule UrbanNomadWeb.ApiGraphql.Schema do
  @moduledoc false
  use Absinthe.Schema

  import_types(UrbanNomadWeb.ApiGraphql.Accounts.Schema)

  query do
    import_fields(:accounts_query)
  end

  @prepend [UrbanNomadWeb.ApiGraphql.Middleware.Authentication]
  @append [UrbanNomadWeb.ApiGraphql.Middleware.TransformErrors]
  def middleware(middleware, _field, %Absinthe.Type.Object{identifier: identifier})
      when identifier in [:query, :subscription, :mutation],
      do: @prepend ++ middleware ++ @append

  def middleware(middleware, _field, _object), do: middleware
end
