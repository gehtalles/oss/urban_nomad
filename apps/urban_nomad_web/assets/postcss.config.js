const path = require('path');

module.exports = ctx => {
  return {
    plugins: [require('autoprefixer')({}), require('postcss-custom-media')({})],
  };
};
