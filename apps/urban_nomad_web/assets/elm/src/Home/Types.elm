module Home.Types exposing (Model, Msg(..), User)

import RemoteData exposing (RemoteData)
import Shared.Api.Types exposing (ApiRemote, ApiResult)
import Shared.Session exposing (Session)


type alias User =
    { displayName : String, email : String }


type alias Model =
    { session : Session
    , message : String
    , user : ApiRemote User
    }


type Msg
    = NoOp
    | ReceiveUser (ApiRemote User)
