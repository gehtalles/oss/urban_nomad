module Home.View exposing (view)

import Home.Types exposing (Model)
import Html exposing (Html, div, text)
import RemoteData exposing (RemoteData)


view : Model -> { title : String, content : Html msg }
view model =
    let
        _ =
            Debug.log "model.user" model.user
    in
    { title = "Home"
    , content =
        div []
            [ case model.user of
                RemoteData.Success data ->
                    div []
                        [ text data.displayName
                        , text " "
                        , text data.email
                        ]

                RemoteData.Failure error ->
                    text ("Error: " ++ Debug.toString error)

                _ ->
                    text "Loading..."
            ]
    }
