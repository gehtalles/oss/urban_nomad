module Home.State exposing (init, subscriptions, toSession, update)

import Graphql.SelectionSet exposing (SelectionSet, with)
import Home.Types exposing (Model, Msg(..), User)
import RemoteData exposing (RemoteData)
import Shared.Api exposing (makeRequest)
import Shared.Api.Schema.Object as SchemaObject
import Shared.Api.Schema.Object.User as SchemaUser
import Shared.Api.Schema.Query as Query
import Shared.Api.Types exposing (ApiResult, ApiUrl, Token)
import Shared.Session exposing (Session)
import Task exposing (Task)


apiUrl =
    Shared.Api.Types.ApiUrl "http://localhost:4000/api"


token =
    Shared.Api.Types.Token "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJ1cmJhbl9ub21hZF9kZXYiLCJleHAiOjE1NDUzODIwNTEsImlhdCI6MTU0Mjk2Mjg1MSwiaXNzIjoidXJiYW5fbm9tYWRfZGV2IiwianRpIjoiZmZmNjlmYmItMjRiYi00ODllLWIzMDYtMjAwM2VlZTFhNmRhIiwibmJmIjoxNTQyOTYyODUwLCJzdWIiOiJ1OjdmNDRiMmE1LTZkMGMtNDc3Ny1hMGI0LTgyYzNmMTM4ODMwOCIsInR5cCI6ImFjY2VzcyJ9.nHvQE2QzXW1muMDXrs4IR4h3wgZIFjVc6ossVz5RxCmZYLCxr53yg8ov0kwDuLfHN2Seg75JaxE8AYNcf2PO5Q"



--


init : Session -> ( Model, Cmd Msg )
init session =
    ( { session = session
      , message = "Welcome to the Home Page!"
      , user = RemoteData.Loading
      }
    , makeRequest apiUrl token (Query.me userSelection) ReceiveUser
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ReceiveUser res ->
            ( { model | user = res }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


toSession : Model -> Session
toSession model =
    model.session


userSelection : SelectionSet User SchemaObject.User
userSelection =
    SchemaUser.selection User
        |> with SchemaUser.displayName
        |> with SchemaUser.email
