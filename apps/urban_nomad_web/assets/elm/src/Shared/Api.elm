module Shared.Api exposing (makeRequest)

-- import Shared.Api.Schema.Mutation as Mutation

import Graphql.Field
import Graphql.Http
import Graphql.Operation exposing (RootMutation, RootQuery)
import Graphql.SelectionSet exposing (SelectionSet, with)
import RemoteData exposing (RemoteData)
import Shared.Api.Schema.Query as Query
import Shared.Api.Types exposing (ApiRemote, ApiUrl(..), Token(..))


makeRequest : ApiUrl -> Token -> Graphql.Field.Field a RootQuery -> (ApiRemote a -> msg) -> Cmd msg
makeRequest (ApiUrl apiUrl) (Token token) sel msg =
    Query.selection identity
        |> with sel
        |> Graphql.Http.queryRequest apiUrl
        |> Graphql.Http.withHeader "authorization" ("Bearer " ++ token)
        |> Graphql.Http.send (RemoteData.fromResult >> msg)



{-
   mutation : ApiUrl -> Token -> Graphql.Field.Field a RootMutation -> (ApiResult a -> Msg) -> Cmd Msg
   mutation (ApiUrl apiUrl) (Token token) sel msg =
       Mutation.selection identity
           |> with sel
           |> Graphql.Http.mutationRequest apiUrl
           |> Graphql.Http.withHeader "authorization" ("Bearer " ++ token)
           |> Graphql.Http.send msg
-}
