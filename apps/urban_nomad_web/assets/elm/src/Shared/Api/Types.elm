module Shared.Api.Types exposing (ApiRemote, ApiResult, ApiUrl(..), Token(..))

import Graphql.Http
import RemoteData exposing (RemoteData)


type alias ApiResult a =
    Result (Graphql.Http.Error a) a


type alias ApiRemote a =
    RemoteData (Graphql.Http.Error a) a


type ApiUrl
    = ApiUrl String


type Token
    = Token String
