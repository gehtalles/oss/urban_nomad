module Main exposing (main)

import Browser
import Core.State exposing (init, subscriptions, update)
import Core.Types exposing (Model, Msg(..))
import Core.View exposing (view)



-- MAIN


main : Program () Model Msg
main =
    Browser.application
        { init = Core.State.init
        , view = Core.View.view
        , update = Core.State.update
        , subscriptions = Core.State.subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }
