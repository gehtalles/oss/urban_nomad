module Core.State exposing (init, subscriptions, update)

import Browser
import Browser.Navigation as Nav
import Core.Route exposing (Route)
import Core.Types exposing (Model, Msg(..))
import Home.State exposing (toSession)
import Shared.Session exposing (Session)
import Url


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    changeRouteTo (Core.Route.fromUrl url) (Core.Types.Redirect (Shared.Session.fromViewer key))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( Ignored, _ ) ->
            ( model, Cmd.none )

        ( LinkClicked urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl (Shared.Session.navKey (toSession model)) (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        ( UrlChanged url, _ ) ->
            changeRouteTo (Core.Route.fromUrl url) model

        ( GotHomeMsg subMsg, Core.Types.Home home ) ->
            Home.State.update subMsg home
                |> updateWith Core.Types.Home GotHomeMsg model

        ( _, _ ) ->
            ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Core.Types.Home home ->
            Sub.map GotHomeMsg (Home.State.subscriptions home)

        _ ->
            Sub.none


changeRouteTo : Maybe Route -> Model -> ( Model, Cmd Msg )
changeRouteTo maybeRoute model =
    let
        session =
            toSession model
    in
    case maybeRoute of
        Nothing ->
            ( Core.Types.NotFound session, Cmd.none )

        Just Core.Route.Home ->
            Home.State.init session
                |> updateWith Core.Types.Home GotHomeMsg model


toSession : Model -> Session
toSession page =
    case page of
        Core.Types.NotFound session ->
            session

        Core.Types.Redirect session ->
            session

        Core.Types.Home home ->
            Home.State.toSession home


updateWith : (subModel -> Model) -> (subMsg -> Msg) -> Model -> ( subModel, Cmd subMsg ) -> ( Model, Cmd Msg )
updateWith toModel toMsg model ( subModel, subCmd ) =
    ( toModel subModel
    , Cmd.map toMsg subCmd
    )
