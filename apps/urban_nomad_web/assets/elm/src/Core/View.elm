module Core.View exposing (view)

import Browser
import Core.Page exposing (Page)
import Core.Page.Blank
import Core.Page.NotFound
import Core.Types exposing (Model(..), Msg(..))
import Home.View exposing (view)
import Html exposing (Html, a, button, div, nav, text, ul)
import Html.Attributes exposing (class, href, id)


view : Model -> Browser.Document Msg
view model =
    let
        viewPage page toMsg config =
            let
                { title, body } =
                    Core.Page.view page config
            in
            { title = title
            , body = List.map (Html.map toMsg) body
            }
    in
    case model of
        Home home ->
            viewPage Core.Page.Home (\_ -> Ignored) (Home.View.view home)

        Redirect _ ->
            viewPage Core.Page.Other (\_ -> Ignored) Core.Page.Blank.view

        NotFound _ ->
            viewPage Core.Page.Other (\_ -> Ignored) Core.Page.NotFound.view


root : Model -> Browser.Document Msg
root model =
    { title = "Foo"
    , body =
        [ div []
            [ header ]
        ]
    }


header : Html Msg
header =
    div []
        [ nav []
            [ div []
                [ a [ href "#", class "brand-logo" ] [ text "BadaDash" ]
                , ul [ id "nav-mobile", class "right hide-on-med-and-down" ] []
                ]
            ]
        ]
