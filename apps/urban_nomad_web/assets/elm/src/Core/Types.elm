module Core.Types exposing (Model(..), Msg(..))

import Browser
import Browser.Navigation as Nav
import Home.Types as Home
import Shared.Session exposing (Session)
import Url


type Model
    = Home Home.Model
    | Redirect Session
    | NotFound Session


type Msg
    = Ignored
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | GotHomeMsg Home.Msg
