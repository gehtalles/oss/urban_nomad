module Core.Page exposing (Page(..), view, viewFooter, viewHeader)

import Browser exposing (..)
import Core.Route as Route
import Html exposing (..)
import Html.Attributes exposing (..)



-- PAGE TYPES


type Page
    = Other
    | Home



-- VIEW


view : Page -> { title : String, content : Html msg } -> Document msg
view page { title, content } =
    { title = title ++ "- Urban Nomad"
    , body =
        viewHeader :: content :: [ viewFooter ]
    }


viewHeader : Html msg
viewHeader =
    header []
        [ div []
            [ span []
                [ text "Title" ]
            , div []
                []
            , nav []
                [ a [ Route.href Route.Home ]
                    [ text "Home" ]
                ]
            ]
        ]


viewFooter : Html msg
viewFooter =
    footer
        [ class "container-fluid" ]
        [ div [ class "container" ]
            [ text "Beta V0.1." ]
        ]
