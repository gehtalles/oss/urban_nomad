import { Elm } from '../elm/src/Main.elm';

const token = document.head.querySelector("meta[name='token']").content;
const node = document.getElementById('root');
const initParams = { flags: { token }, node };
Elm.Main.init();
