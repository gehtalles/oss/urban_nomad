defmodule UNAccounts.DefaultImpl do
  @moduledoc false
  import Ecto.Query, warn: false

  alias __MODULE__
  alias Comeonin.Bcrypt
  alias Ecto.{Changeset, Multi}

  alias UrbanNomad.{
    EmailAlreadyRegisteredError,
    InvalidDataError,
    PasswordReset,
    User,
    UserNotFoundError,
    WrongCredentialsError
  }

  alias DefaultImpl.Helper
  alias DefaultImpl.PasswordReset, as: ImplPasswordReset
  alias DefaultImpl.Repo
  alias DefaultImpl.User, as: ImplUser

  @behaviour UNAccounts.Impl

  @spec get_user_by_id(String.t()) :: {:ok, User.t()} | {:error, UserNotFoundError.t()}
  def get_user_by_id(id) do
    case ImplUser |> Repo.get(id) do
      nil -> {:error, UserNotFoundError.exception(error: "id not found #{id}")}
      user -> {:ok, user |> Helper.to_domain()}
    end
  end

  @spec get_user_by_email(String.t()) :: {:ok, User.t()} | {:error, UserNotFoundError.t()}
  def get_user_by_email(email) do
    case ImplUser |> Repo.get_by(email: email) do
      nil -> {:error, UserNotFoundError.exception(error: "email not found #{email}")}
      user -> {:ok, user |> Helper.to_domain()}
    end
  end

  @spec create_user(map) ::
          {:ok, User.t()} | {:error, InvalidDataError.t() | EmailAlreadyRegisteredError.t()}
  def create_user(attrs \\ %{}) do
    case %ImplUser{} |> ImplUser.changeset(attrs) |> Repo.insert() do
      {:ok, user} ->
        {:ok, Helper.to_domain(user)}

      {:error, %Changeset{} = changeset} ->
        changeset |> Helper.transform_errors() |> Helper.handle_errors(changeset)
    end
  end

  @spec authenticate_by_email_and_pass(String.t(), String.t()) ::
          {:ok, User.t()} | {:error, WrongCredentialsError.t()}
  def authenticate_by_email_and_pass(email, password)
      when is_binary(email) and is_binary(password) do
    with %ImplUser{} = user <- ImplUser |> Repo.get_by(email: email),
         true <- Helper.check_password(user.password_hash, password) do
      {:ok, Helper.to_domain(user)}
    else
      false ->
        Bcrypt.dummy_checkpw()
        {:error, WrongCredentialsError.exception(errors: "wrong password")}

      nil ->
        {:error, WrongCredentialsError.exception(errors: "email not found #{email}")}
    end
  end

  @spec create_password_reset_code(String.t()) ::
          {:ok, PasswordReset.t()} | {:error, UserNotFoundError.t()}
  def create_password_reset_code(email) do
    with %ImplUser{} = user <- ImplUser |> Repo.get_by(email: email),
         {:ok, one_day_from_now} <- Helper.one_day_from_now(),
         {:ok, %ImplPasswordReset{} = password_reset} <-
           %ImplPasswordReset{}
           |> ImplPasswordReset.changeset(%{user_id: user.id, expires_at: one_day_from_now})
           |> Repo.insert() do
      {:ok, password_reset |> Helper.to_domain()}
    else
      nil -> {:error, UserNotFoundError.exception(error: "not found by email #{email}")}
      {:error, err} -> {:error, InvalidDataError.exception(errors: err)}
    end
  end

  @spec validate_password_reset_code(String.t()) ::
          {:ok, PasswordReset.t()} | {:error, InvalidDataError.t()}
  def validate_password_reset_code(password_reset_code) do
    with %ImplPasswordReset{} = password_reset <-
           ImplPasswordReset |> Repo.get(password_reset_code),
         false <- Helper.expired?(password_reset) do
      {:ok, password_reset |> Helper.to_domain()}
    else
      _ ->
        {:error,
         InvalidDataError.exception(
           errors: "password_reset_code not found #{password_reset_code}"
         )}
    end
  end

  @spec update_user_password(String.t(), map) :: {:ok, User.t()} | {:error, InvalidDataError.t()}
  def update_user_password(password_reset_code, update) do
    password_reset = ImplPasswordReset |> Repo.get(password_reset_code) |> Repo.preload(:user)
    %{user: %ImplUser{} = user} = password_reset

    transaction =
      Multi.new()
      |> Multi.update(:user, ImplUser.reset_password_changeset(user, update))
      |> Multi.delete_all(:password_reset, Ecto.assoc(user, :password_reset))

    case Repo.transaction(transaction) do
      {:ok, %{user: user}} -> {:ok, user |> Helper.to_domain()}
      {:error, operation, value, _} -> InvalidDataError.exception(errors: [operation, value])
    end
  end
end
