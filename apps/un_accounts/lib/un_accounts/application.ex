defmodule UNAccounts.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      UNAccounts.DefaultImpl.Repo
    ]

    opts = [strategy: :one_for_one, name: UNAccounts.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
