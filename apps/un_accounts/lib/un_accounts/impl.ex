defmodule UNAccounts.Impl do
  @moduledoc false

  alias UrbanNomad.{
    EmailAlreadyRegisteredError,
    InvalidDataError,
    PasswordReset,
    User,
    UserNotFoundError,
    WrongCredentialsError
  }

  @callback get_user_by_id(String.t()) :: {:ok, User.t()} | {:error, UserNotFoundError.t()}

  @callback create_user(map) ::
              {:ok, User.t()} | {:error, InvalidDataError.t() | EmailAlreadyRegisteredError.t()}
  @callback authenticate_by_email_and_pass(String.t(), String.t()) ::
              {:ok, User.t()} | {:error, WrongCredentialsError.t()}
  @callback create_password_reset_code(String.t()) ::
              {:ok, PasswordReset.t()} | {:error, UserNotFoundError.t()}

  @callback validate_password_reset_code(String.t()) ::
              {:ok, PasswordReset.t()} | {:error, InvalidDataError.t()}

  @callback update_user_password(String.t(), map) ::
              {:ok, User.t()} | {:error, InvalidDataError.t()}
end
