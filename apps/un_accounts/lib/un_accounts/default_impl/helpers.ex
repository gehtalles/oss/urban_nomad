defmodule UNAccounts.DefaultImpl.Helper do
  @moduledoc false

  alias Comeonin.Bcrypt
  alias Ecto.Changeset
  alias UNAccounts.DefaultImpl.PasswordReset, as: ImplPasswordReset
  alias UNAccounts.DefaultImpl.User, as: ImplUser

  alias UrbanNomad.{
    EmailAlreadyRegisteredError,
    InvalidDataError,
    PasswordReset,
    User
  }

  @spec check_password(String.t(), String.t()) :: true | false
  def check_password(password_hash, password), do: password |> Bcrypt.checkpw(password_hash)

  @spec handle_errors(map, Changeset.t()) ::
          {:error, EmailAlreadyRegisteredError.t() | InvalidDataError.t()}
  def handle_errors(%{email: ["is_already_registered"]}, changeset),
    do:
      {:error,
       EmailAlreadyRegisteredError.exception(email: Changeset.get_change(changeset, :email))}

  def handle_errors(_, changeset),
    do: {:error, InvalidDataError.exception(errors: changeset.errors)}

  @spec to_domain(ImplUser.t()) :: User.t() | nil
  def to_domain(%ImplUser{id: id, email: email, display_name: display_name}) do
    %User{id: id, email: email, display_name: display_name}
  end

  @spec to_domain(ImplPasswordReset.t()) :: PasswordReset.t() | nil
  def to_domain(%ImplPasswordReset{id: id}) do
    %PasswordReset{code: id}
  end

  def to_domain(nil), do: nil

  @spec transform_errors(Changeset.t()) :: map
  def transform_errors(changeset) do
    Changeset.traverse_errors(changeset, fn {message, opts} ->
      Enum.reduce(opts, message, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
  end

  @spec one_day_from_now() :: {:ok, DateTime.t()} | {:error, atom}
  def one_day_from_now,
    do:
      DateTime.utc_now()
      |> DateTime.to_unix()
      |> Kernel.+(86_400)
      |> DateTime.from_unix()

  @spec expired?(ImplPasswordReset.t(), DateTime.t()) :: true | false
  def expired?(%ImplPasswordReset{expires_at: expires_at}, compare_with \\ DateTime.utc_now()),
    do: expires_at |> DateTime.compare(compare_with) |> _expired?

  defp _expired?(:gt), do: false
  defp _expired?(_), do: true
end
