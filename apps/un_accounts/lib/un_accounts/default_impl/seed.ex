defmodule UNAccounts.DefaultImpl.Seed do
  @moduledoc false

  alias Comeonin.Bcrypt
  alias UNAccounts.DefaultImpl.Repo
  alias UNAccounts.DefaultImpl.User
  require Logger

  @user_passwd Bcrypt.hashpwsalt("urbannomad")

  def run(env) do
    Logger.info("== Running seed UNAccounts")
    seed(env)
    Logger.info("== Seeded UNAccounts")
  end

  def seed(:dev) do
    seed_users!()
  end

  def seed(:prod) do
  end

  defp seed_users!() do
    Repo.delete_all(User)

    users = [
      user("Harry Potter", "user@un.app", @user_passwd)
    ]

    Repo.insert_all(User, users, on_conflict: :nothing, conflict_target: [:email])

    Logger.info("Inserted #{length(users)} users.")
  end

  defp user(display_name, email, pwd_hash) do
    now = DateTime.truncate(DateTime.utc_now(), :second)

    %{
      display_name: display_name,
      email: email,
      password_hash: pwd_hash,
      inserted_at: now,
      updated_at: now
    }
  end
end
