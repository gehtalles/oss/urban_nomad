defmodule UNAccounts.DefaultImpl.User do
  @moduledoc false

  use Ecto.Schema

  import Ecto.Changeset

  alias Comeonin.Bcrypt
  alias Ecto.Changeset
  alias UNAccounts.DefaultImpl.PasswordReset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field(:email, :string)
    field(:display_name, :string)
    field(:password, :string, virtual: true)
    field(:password_hash, :string)
    has_one(:password_reset, PasswordReset)

    timestamps(type: :utc_datetime)
  end

  @type t :: %__MODULE__{
          id: String.t(),
          display_name: String.t(),
          email: String.t(),
          password: String.t()
        }

  @cast_fields ~w(email password display_name)a
  @required_fields ~w(email password display_name)a
  def changeset(user, attrs) do
    user
    |> cast(attrs, @cast_fields)
    |> validate_required(@required_fields)
    |> put_password_hash()
    |> unique_constraint(:email, message: "is_already_registered")
  end

  @cast_fields ~w(password)a
  @required_fields ~w(password)a
  def reset_password_changeset(user, attrs) do
    user
    |> cast(attrs, @cast_fields)
    |> validate_required(@required_fields)
    |> put_password_hash()
  end

  defp put_password_hash(%Changeset{valid?: true, changes: %{password: password}} = changeset) do
    put_change(changeset, :password_hash, Bcrypt.hashpwsalt(password))
  end

  defp put_password_hash(changeset), do: changeset
end
