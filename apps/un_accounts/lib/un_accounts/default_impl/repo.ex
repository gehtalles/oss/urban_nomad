defmodule UNAccounts.DefaultImpl.Repo do
  @moduledoc false

  use Ecto.Repo,
    otp_app: :un_accounts,
    adapter: Ecto.Adapters.Postgres
end
