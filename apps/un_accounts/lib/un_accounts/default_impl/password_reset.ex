defmodule UNAccounts.DefaultImpl.PasswordReset do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  alias UNAccounts.DefaultImpl.User

  @type t :: %__MODULE__{
          id: String.t(),
          expires_at: DateTime.t()
        }

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "password_resets" do
    field(:expires_at, :utc_datetime)
    belongs_to(:user, User)

    timestamps(type: :utc_datetime)
  end

  @cast_fields ~w(expires_at user_id)a
  @required_fields ~w(expires_at user_id)a
  def changeset(password_reset, attrs) do
    password_reset
    |> cast(attrs, @cast_fields)
    |> validate_required(@required_fields)
  end
end
