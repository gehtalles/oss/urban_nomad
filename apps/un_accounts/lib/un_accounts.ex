defmodule UNAccounts do
  @moduledoc """
  Internal API for working with accounts.
  """
  alias UNAccounts.DefaultImpl

  alias UrbanNomad.{
    EmailAlreadyRegisteredError,
    InvalidDataError,
    PasswordReset,
    User,
    UserNotFoundError,
    WrongCredentialsError
  }

  @behaviour UNAccounts.Impl

  @spec get_user_by_id(String.t()) :: {:ok, User.t()} | {:error, UserNotFoundError.t()}
  def get_user_by_id(user_id), do: impl().get_user_by_id(user_id)

  @spec create_user(map) ::
          {:ok, User.t()} | {:error, InvalidDataError.t() | EmailAlreadyRegisteredError.t()}
  def create_user(attrs \\ %{}), do: impl().create_user(attrs)

  @spec authenticate_by_email_and_pass(String.t(), String.t()) ::
          {:ok, User.t()} | {:error, WrongCredentialsError.t()}
  def authenticate_by_email_and_pass(email, password),
    do: impl().authenticate_by_email_and_pass(email, password)

  @spec create_password_reset_code(String.t()) ::
          {:ok, PasswordReset.t()} | {:error, UserNotFoundError.t()}
  def create_password_reset_code(email),
    do: impl().create_password_reset_code(email)

  @spec validate_password_reset_code(String.t()) ::
          {:ok, PasswordReset.t()} | {:error, InvalidDataError.t()}
  def validate_password_reset_code(password_reset_code),
    do: impl().validate_password_reset_code(password_reset_code)

  @spec update_user_password(String.t(), map) :: {:ok, User.t()} | {:error, InvalidDataError.t()}
  def update_user_password(password_reset_code, update),
    do: impl().update_user_password(password_reset_code, update)

  defp impl, do: Application.get_env(:un_accounts, :impl, DefaultImpl)
end
