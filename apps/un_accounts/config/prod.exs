use Mix.Config

config :un_accounts, UNAccounts.DefaultImpl.Repo,
  pool_size: 10,
  ssl: true
