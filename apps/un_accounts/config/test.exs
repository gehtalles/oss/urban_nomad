use Mix.Config

config :un_accounts, UNAccounts.DefaultImpl.Repo,
  username: "postgres",
  password: "postgres",
  database: "urban_nomad_test",
  hostname: System.get_env("POSTGRES_SRV") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :bcrypt_elixir, :log_rounds, 4
