use Mix.Config

config :un_accounts, UNAccounts.DefaultImpl.Repo,
  username: "postgres",
  password: "postgres",
  database: "urban_nomad_dev",
  hostname: System.get_env("POSTGRES_SRV") || "localhost",
  pool_size: 10
