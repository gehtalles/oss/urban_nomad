use Mix.Config

config :un_accounts, ecto_repos: [UNAccounts.DefaultImpl.Repo]

import_config "#{Mix.env()}.exs"
