alias UNAccounts.DefaultImpl.Seed, as: UsersSeed

env = (System.get_env("MIX_ENV") || "dev") |> String.to_atom()

UsersSeed.run(env)
