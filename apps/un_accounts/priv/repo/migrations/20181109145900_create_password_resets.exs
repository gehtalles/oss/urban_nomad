defmodule UNAccounts.DefaultImpl.CreatePasswordResets do
  use Ecto.Migration

  def change do
    create table(:password_resets, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:user_id, references(:users, on_delete: :nothing, type: :binary_id), null: false)
      add(:expires_at, :utc_datetime, null: false)

      timestamps(type: :utc_datetime)
    end
  end
end
