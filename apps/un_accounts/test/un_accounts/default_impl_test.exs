defmodule UNAccounts.DefaultImplTest do
  use UNAccounts.DataCase, async: true

  alias UNAccounts.DefaultImpl

  alias UrbanNomad.{
    EmailAlreadyRegisteredError,
    InvalidDataError,
    PasswordReset,
    User,
    UserNotFoundError,
    WrongCredentialsError
  }

  @valid_attrs valid_user_attrs()
  @invalid_attrs %{email: nil, password: nil}

  describe "get_user_by_id/1" do
    test "returns the user with given id" do
      %User{id: user_id} = create_user()
      assert {:ok, %User{id: ^user_id}} = DefaultImpl.get_user_by_id(user_id)
    end

    test "returns nil with non existing id" do
      alias Ecto.UUID

      assert {:error, %UserNotFoundError{}} = UUID.generate() |> DefaultImpl.get_user_by_id()
    end
  end

  describe "get_user_by_email/1" do
    test "returns the user with given id" do
      %User{email: email} = create_user()
      assert {:ok, %User{email: ^email}} = DefaultImpl.get_user_by_email(email)
    end

    test "returns nil with non existing id" do
      assert {:error, %UserNotFoundError{}} = DefaultImpl.get_user_by_email("email")
    end
  end

  describe "create_user/1" do
    test "with valid data creates a user" do
      assert {:ok, %User{id: id}} = DefaultImpl.create_user(@valid_attrs)
    end

    test "with invalid data returns error changeset" do
      assert {:error, %InvalidDataError{}} = DefaultImpl.create_user(@invalid_attrs)
    end

    test "when email has already been taken" do
      {:ok, _} = DefaultImpl.create_user(@valid_attrs)

      assert {:error, %EmailAlreadyRegisteredError{}} = DefaultImpl.create_user(@valid_attrs)
    end
  end

  describe "authenticate_by_email_and_pass/2" do
    test "returns the user with valid credentials" do
      %User{id: user_id} = create_user(@valid_attrs)

      assert {:ok, %User{id: ^user_id}} =
               DefaultImpl.authenticate_by_email_and_pass(
                 @valid_attrs.email,
                 @valid_attrs.password
               )
    end

    test "returns error with invalid password" do
      create_user(@valid_attrs)

      assert {:error, %WrongCredentialsError{}} =
               DefaultImpl.authenticate_by_email_and_pass(@valid_attrs.email, "wrong pass")
    end

    test "returns error with invalid email" do
      create_user(@valid_attrs)

      assert {:error, %WrongCredentialsError{}} =
               DefaultImpl.authenticate_by_email_and_pass(
                 "unknown_user",
                 @valid_attrs.password
               )
    end
  end

  describe "create_password_reset_code/1" do
    test "with valid user" do
      %User{} = user = create_user(@valid_attrs)
      assert {:ok, %PasswordReset{}} = DefaultImpl.create_password_reset_code(user.email)
    end

    test "when user unknown" do
      assert {:error, %UserNotFoundError{}} =
               DefaultImpl.create_password_reset_code("unknown_user")
    end
  end

  describe "validate_password_reset_code/1" do
    test "with valid code" do
      %PasswordReset{code: password_reset_code} = create_password_reset()

      assert {:ok, %PasswordReset{}} =
               DefaultImpl.validate_password_reset_code(password_reset_code)
    end

    test "when reset code is unknown" do
      alias Ecto.UUID

      assert {:error, %InvalidDataError{}} =
               DefaultImpl.validate_password_reset_code(UUID.generate())
    end
  end

  describe "reset_password/1" do
    test "with valid data" do
      %User{id: user_id, email: user_email} = create_user()
      %PasswordReset{code: password_reset_code} = create_password_reset(user_email)

      update = %{
        password: "123",
        password_confirmation: "123"
      }

      assert {:ok, %User{id: ^user_id}} =
               DefaultImpl.update_user_password(password_reset_code, update)

      assert {:error, %InvalidDataError{}} =
               DefaultImpl.validate_password_reset_code(password_reset_code)
    end

    test "when user unknown" do
      assert {:error, %UserNotFoundError{}} =
               DefaultImpl.create_password_reset_code("unknown_user")
    end
  end
end
