defmodule UNAccounts.TestHelper do
  @moduledoc false

  alias UNAccounts.DefaultImpl

  def valid_user_attrs do
    salt = random_string()

    %{
      display_name: "Jane Doe-#{salt}",
      email: "user#{salt}@urbannomad.app",
      password: "$ecret$pass"
    }
  end

  def valid_password_reset_attrs do
    %{
      code: random_string()
    }
  end

  def create_password_reset(email \\ create_user() |> Map.get(:email)) do
    {:ok, password_reset} = DefaultImpl.create_password_reset_code(email)

    password_reset
  end

  def create_user(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(valid_user_attrs())
      |> DefaultImpl.create_user()

    user
  end

  defp random_string do
    8
    |> :crypto.strong_rand_bytes()
    |> Base.encode16()
    |> String.downcase()
  end
end
