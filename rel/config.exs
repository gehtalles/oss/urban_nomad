use Mix.Releases.Config,
  default_release: :default,
  default_environment: Mix.env()

environment :dev do
  set(dev_mode: true)
  set(include_erts: false)
  set(cookie: :test)
end

environment :prod do
  set(include_erts: true)
  set(include_src: false)

  set(
    cookie: :crypto.hash(:sha256, System.get_env("COOKIE")) |> Base.encode16() |> String.to_atom()
  )

  set(
    config_providers: [
      {Mix.Releases.Config.Providers.Elixir, ["${RELEASE_ROOT_DIR}/etc/runtime.exs"]}
    ]
  )

  set(pre_start_hooks: "rel/hooks/pre_start")
end

release :urban_nomad do
  set(version: current_version(:urban_nomad))

  set(
    commands: [
      migrate: "rel/commands/migrate",
      seed: "rel/commands/seed"
    ]
  )

  set(
    overlays: [
      {:copy, "rel/config/runtime.exs", "etc/runtime.exs"},
      {:copy, "rel/config/staging.exs", "etc/staging.exs"}
    ]
  )

  set(
    applications: [
      :runtime_tools,
      un_accounts: :permanent,
      un_notifications: :permanent,
      un_shared: :permanent,
      urban_nomad: :permanent,
      urban_nomad_web: :permanent
    ]
  )
end
