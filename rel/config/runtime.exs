use Mix.Config

config :un_accounts, UNAccounts.DefaultImpl.Repo, url: System.get_env("DATABASE_URL")

config :un_notifications, UNNotifications.Emails.DefaultImpl.Mailer,
  api_key: System.get_env("SEND_GRID_API_KEY")

config :urban_nomad_web, UrbanNomadWeb.Endpoint,
  http: [:inet6, port: System.get_env("WEB_PORT")],
  url: [host: System.get_env("WEB_HOSTNAME"), port: 443, scheme: "https"]

config :urban_nomad_web, UrbanNomadWeb.Session.Tokenizer,
  issuer: "urban_nomad_prod",
  secret_key: System.get_env("TOKEN_SECRET_KEY")
