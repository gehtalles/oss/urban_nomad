FROM elixir:1.7

MAINTAINER Johannes Hellmut Troeger <jht@gehtalles.at>

ENV REFRESHED_AT=2018-11-02 \
    # Set this so that CTRL+G works properly
    TERM=xterm

RUN curl -sL https://deb.nodesource.com/setup_11.x | bash - && \
    apt-get update -y && apt-get install nodejs -y

RUN mix local.hex --force && \
    mix local.rebar --force

CMD ["/bin/sh"]
