defmodule UrbanNomad.Umbrella.MixProject do
  use Mix.Project

  def project do
    [
      aliases: aliases(),
      apps_path: "apps",
      app: :urban_nomad,
      deps: deps(),
      dialyzer: dialyzer(),
      start_permanent: Mix.env() == :prod
    ]
  end

  # Dependencies listed here are available only for this
  # project and cannot be accessed from applications inside
  # the apps folder.
  #
  # Run "mix help deps" for examples and options.
  defp deps do
    [
      {:credo, "~> 1.0.0-rc", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0.0-rc", only: [:dev, :test], runtime: false},
      {:distillery, "~> 2.0", runtime: false},
      {:ex_doc, "~> 0.14", only: :dev, runtime: false}
    ]
  end

  defp dialyzer do
    [
      plt_add_deps: :app_tree,
      plt_file: {:no_warn, "priv/plts/eventstore.plt"}
    ]
  end

  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "ecto.seed"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "ecto.seed": ["run apps/un_accounts/priv/repo/seeds.exs"],
      test: ["ecto.drop --quiet", "ecto.create --quiet", "ecto.migrate", "test"],
      lint: [
        "clean",
        "compile --warnings-as-errors",
        "credo",
        "format --check-formatted",
        "dialyzer --halt-exit-status",
        "mix xref unreachable --include-siblings",
        "mix xref deprecated --include-siblings"
      ]
    ]
  end
end
